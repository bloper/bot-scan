class bot{
    _motion:motion;
    _map = [];
    _y:number;
    _x:number;
    _direction:number = 0;
    constructor(y,x){
        this._y = y;
        this._x = x;
        this._motion = new motion;
    }
    scan(){
        let canMove = true;
        while(canMove){
            let myMove = this._motion.move(this._y,this._x);
            if(!myMove){
                //can't move
                this._map.push({y:myMove[0],x:myMove[1],v:-1});
                this._direction = this._motion.rotate();
            }else{
                //can move
                this._map.push({y:myMove[0],x:myMove[1],v:1});
            }
            
        }
    }
}