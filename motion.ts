class motion implements imotion{
    _map : map;
    _direction : number;
    _directions = [{y:0,x:1,info:"right"},{y:1,x:0,info:"down"},{y:0,x:-1,info:"left"},{y:-1,x:0,info:"up"}];
    constructor(){
        this._map = new map(66,66);
    }
    move(y:number,x:number){
        y += this._directions[this._direction].y;
        x += this._directions[this._direction].x;
        let m = this._map.getMap();
        if(m[y][x]==0 || m[y][x]==1){
            return [y,x];
        }
        return false;
    }
    rotate(){
        this._direction++;
        if(this._direction>=this._directions.length){
            this._direction=0;
        }
        return this._direction[this._direction].info;
    }
}