class map{
    _map = [];
    constructor(row:number,column:number){
        for(let y=0;y<row;y++){
            var arr = [];
            for(let x=0;x<column;x++){
                let val = 0;
                if(y==0 || x==0 || y==row-1 || x==column-1){
                    val = -1;
                }
                arr.push(val);
            }
            this._map.push(arr);
        }
        //set start position
        this._map[1][1] = 1;
    }
    getMap(){
        return this._map;
    }
}